import os
import sys
import json
from urllib import request
from http.client import HTTPConnection

from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import *
from PyQt5.uic import *


def resource_path(relative_path):
    """ Get absolute path to resources, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


ui_file_name = "resources/sony.ui"
ui_file = resource_path(ui_file_name)
ui, BaseClass = loadUiType(ui_file)


class MainApp(QMainWindow, ui):
    def __init__(self, parent=None):
        super(MainApp, self).__init__(parent)
        QMainWindow.__init__(self)
        self.active = "active"
        self.standby = "standby"
        self.unknown = "unknown"
        self.orange = "color: orange;"
        self.green = "color: green;"
        self.yellow = "color: yellow;"
        self.gray = "color: gray;"

        self.ping = False
        self.power_status = ""
        self.volume = 0
        self.volume_min = 0
        self.volume_max = 0

        self.setupUi(self)
        self.init_ui()
        self.handle_buttons()
        self.timer = QTimer()
        self.timer.setInterval(100)
        self.timer.timeout.connect(self.recurring_timer)
        self.timer.start()

    def init_ui(self):
        self.get_power_status()
        self.update_volume_bar()
        pass

    def is_power_on(self):
        self.get_power_status()
        if self.power_status == self.active:
            return True
        else:
            return False

    def update_volume_bar(self):
        if self.is_power_on():
            self.get_volume()

    def recurring_timer(self):
        self.update_volume_bar()
        if self.ping:
            self.status.setStyleSheet(f"""{self.yellow}""")
            self.status.update()
            self.status.repaint()
            self.ping = False
        else:
            self.get_power_status()

    def handle_buttons(self):
        self.volumeDownButton.clicked.connect(lambda: self.set_command("volumeDownButton"))
        self.volumeUpButton.clicked.connect(lambda: self.set_command("volumeUpButton"))
        self.channelUp.clicked.connect(lambda: self.set_command("channelUp"))
        self.channelDown.clicked.connect(lambda: self.set_command("channelDown"))
        self.homeButton.clicked.connect(lambda: self.set_command("homeButton"))
        self.confirmButton.clicked.connect(lambda: self.set_command("confirmButton"))
        self.upButton.clicked.connect(lambda: self.set_command("upButton"))
        self.downButton.clicked.connect(lambda: self.set_command("downButton"))
        self.leftButton.clicked.connect(lambda: self.set_command("leftButton"))
        self.rightButton.clicked.connect(lambda: self.set_command("rightButton"))
        self.powerButton.clicked.connect(lambda: self.set_command("powerButton"))
        self.returnButton.clicked.connect(lambda: self.set_command("returnButton"))
        self.guideButton.clicked.connect(lambda: self.set_command("guideButton"))
        self.optionsButton.clicked.connect(lambda: self.set_command("optionsButton"))
        self.inputButton.clicked.connect(lambda: self.set_command("inputButton"))
        self.syncButton.clicked.connect(lambda: self.set_command("syncButton"))
        self.muteButton.clicked.connect(lambda: self.set_command("muteButton"))
        self.exitButton.clicked.connect(lambda: self.set_command("exitButton"))
        self.displayButton.clicked.connect(lambda: self.set_command("displayButton"))
        self.tvButton.clicked.connect(lambda: self.set_command("tvButton"))
        self.hdmi1Button.clicked.connect(lambda: self.set_command("hdmi1Button"))
        self.hdmi2Button.clicked.connect(lambda: self.set_command("hdmi2Button"))
        self.hdmi3Button.clicked.connect(lambda: self.set_command("hdmi3Button"))
        self.hdmi4Button.clicked.connect(lambda: self.set_command("hdmi4Button"))
        self.previousChannelButton.clicked.connect(lambda: self.set_command("previousChannelButton"))

    def set_command(self, command):
        switcher = {
            "volumeDownButton":      "AAAAAQAAAAEAAAATAw==",
            "volumeUpButton":        "AAAAAQAAAAEAAAASAw==",
            "channelUp":             "AAAAAQAAAAEAAAAQAw==",
            "channelDown":           "AAAAAQAAAAEAAAARAw==",
            "homeButton":            "AAAAAQAAAAEAAABgAw==",
            "confirmButton":         "AAAAAQAAAAEAAABlAw==",
            "upButton":              "AAAAAQAAAAEAAAB0Aw==",
            "downButton":            "AAAAAQAAAAEAAAB1Aw==",
            "leftButton":            "AAAAAQAAAAEAAAA0Aw==",
            "rightButton":           "AAAAAQAAAAEAAAAzAw==",
            "returnButton":          "AAAAAgAAAJcAAAAjAw==",
            "guideButton":           "AAAAAgAAAKQAAABbAw==",
            "optionsButton":         "AAAAAgAAAJcAAAA2Aw==",
            "inputButton":           "AAAAAQAAAAEAAAAlAw==",
            "syncButton":            "AAAAAgAAABoAAABYAw==",
            "muteButton":            "AAAAAQAAAAEAAAAUAw==",
            "exitButton":            "AAAAAQAAAAEAAABjAw==",
            "displayButton":         "AAAAAQAAAAEAAAA6Aw==",
            "tvButton":              "AAAAAQAAAAEAAAAkAw==",
            "hdmi1Button":           "AAAAAgAAABoAAABaAw==",
            "hdmi2Button":           "AAAAAgAAABoAAABbAw==",
            "hdmi3Button":           "AAAAAgAAABoAAABcAw==",
            "hdmi4Button":           "AAAAAgAAABoAAABdAw==",
            "previousChannelButton": "AAAAAQAAAAEAAAA7Aw==",
        }
        if command == "powerButton":
            if self.power_status == self.standby:
                # TODO: WOL
                pass
            else:
                self.post_call("AAAAAQAAAAEAAAAvAw==")
        else:
            self.post_call(switcher.get(command, ""))
        self.update_volume_bar()

    def set_signal(self):
        self.ping = True

    def post_call(self, command):
        if self.is_power_on():
            self.set_signal()
            ip = self.tvIP.text()
            data = bytearray(f"""<s:Envelope xmlns:s='http://schemas.xmlsoap.org/soap/envelope/'
    s:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'><s:Body><u:X_SendIRCC
    xmlns:u='urn:schemas-sony-com:service:IRCC:1'><IRCCCode>{command}</IRCCCode></u:X_SendIRCC>
    </s:Body></s:Envelope>""", 'utf-8')
            r = request.Request(f"http://{ip}/sony/IRCC", data=data)
            r.add_header('Content-Type', 'text/xml')
            r.add_header('charset', 'UTF-8')
            r.add_header('SOAPACTION', 'urn:schemas-sony-com:service:IRCC:1')
            request.urlopen(r)

    def unknown_status(self):
        self.power_status = self.unknown
        self.status.setStyleSheet(f"""{self.gray}""")

    def get_power_status(self):
        ip = self.tvIP.text()
        try:
            conn = HTTPConnection(ip, 80, 0.1)
            conn.request("HEAD", "/")
            conn.close()
            up = True
        except IOError:
            up = False

        if up:
            data = bytearray("""{"method":"getPowerStatus","id":50,"params":[],"version":"1.0"}""", 'utf-8')
            r = request.Request(f"http://{ip}/sony/system", data=data)
            with request.urlopen(r) as result:
                try:
                    self.power_status = json.loads(result.read().decode('utf-8'))['result'][0]['status']
                    switcher = {
                        self.standby: self.orange,
                        self.active: self.green,
                    }
                    style = switcher.get(self.power_status, self.gray)
                    self.status.setStyleSheet(f"""{style}""")
                except KeyError or ValueError or TypeError:
                    self.unknown_status()
        else:
            self.unknown_status()

    def get_volume(self):
        ip = self.tvIP.text()
        data = bytearray("""{"method":"getVolumeInformation","id":33,"params":[],"version":"1.0"}""", 'utf-8')
        r = request.Request(f"http://{ip}/sony/audio", data=data)
        with request.urlopen(r) as result:
            try:
                speaker = json.loads(result.read().decode('utf-8'))['result'][0][0]
                self.volume = int(speaker['volume'])
                self.volume_min = int(speaker['minVolume'])
                self.volume_max = int(speaker['maxVolume'])
                self.volumeBar.setValue(self.volume)
                self.volumeBar.setMinimum(self.volume_min)
                self.volumeBar.setMaximum(self.volume_max)
                self.volumeBar.update()
                self.volumeBar.repaint()
            except KeyError or ValueError or TypeError:
                pass


def main():
    app = QApplication(sys.argv)
    window = MainApp()
    window.show()
    app.exec_()


if __name__ == '__main__':
    main()
